﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataLayer
{
    public class StatefulArticle : Article
    {
        public ArticleStates.ArticleState articalState;
        ArticleStates.ArticleState Submitted;
        ArticleStates.ArticleState WriterApproved;
        ArticleStates.ArticleState MediaManagerApproved;
        ArticleStates.ArticleState Published;
        ArticleStates.ArticleState Rejected;

        public List<ArticleStates.ArticleState> allStates;

        public StatefulArticle() {
            this.Submitted = new ArticleStates.Submitted(this);
            this.WriterApproved = new ArticleStates.WriterApproved(this);
            this.MediaManagerApproved = new ArticleStates.MediaManagerApproved(this);
            this.Published = new ArticleStates.Published(this);
            this.Rejected = new ArticleStates.Rejected(this);

            allStates = new List<ArticleStates.ArticleState>();
            allStates.Add(Submitted);
            allStates.Add(WriterApproved);
            allStates.Add(MediaManagerApproved);
            allStates.Add(Published);
            allStates.Add(Rejected);

            this.articalState = Submitted;
        }

        public StatefulArticle(Article a, bool isNew) {
            this.Submitted = new ArticleStates.Submitted(this);
            this.WriterApproved = new ArticleStates.WriterApproved(this);
            this.MediaManagerApproved = new ArticleStates.MediaManagerApproved(this);
            this.Published = new ArticleStates.Published(this);
            this.Rejected = new ArticleStates.Rejected(this);

            this.articleid = a.articleid;
            this.article_text = a.article_text;
            this.article_title = a.article_title;
            this.author = a.author;
            this.date_posted = a.date_posted;
            this.workflow_id = a.workflow_id;
            this.state = a.state;

            allStates = new List<ArticleStates.ArticleState>();
            allStates.Add(Submitted);
            allStates.Add(WriterApproved);
            allStates.Add(MediaManagerApproved);
            allStates.Add(Published);
            allStates.Add(Rejected);

            if (isNew) { this.articalState = new ArticleStates.Submitted(this); }
            else { this.articalState = getArticleState(a, this);}
        }

        private ArticleStates.ArticleState getArticleState(Article a, StatefulArticle sa) {
            switch (new ArticleActions().getArticleState(a.articleid).state_name) {
                case "Submitted":
                    return new ArticleStates.Submitted(sa);
                case "Writer Approved":
                    return new ArticleStates.WriterApproved(sa);
                case "Media Manager Approved":
                    return new ArticleStates.MediaManagerApproved(sa);
                case "Published":
                    return new ArticleStates.Published(sa);
                case "Rejected":
                    return new ArticleStates.Rejected(sa);
                default:
                    return null;
            }
        }

        public void setArticleState(ArticleStates.ArticleState newState) {
            this.articalState = newState;
        }

        public void writerApprove(int workflowId) {
            articalState.writerApprove(workflowId);
        }

        public void mediaManagerApprove(int workflowId) {
            articalState.mediaManagerApprove(workflowId);
        }

        public void reject(int workflowId) {
            articalState.reject(workflowId);
        }

        public void publish(int workflowId) {
            articalState.publish(workflowId);
        }

        public ArticleStates.ArticleState getSubmittedState() {
            return this.Submitted;
        }

        public ArticleStates.ArticleState getRejectedState()
        {
            return this.Rejected;
        }

        public ArticleStates.ArticleState getWriterApproveState()
        {
            return this.WriterApproved;
        }

        public ArticleStates.ArticleState getMediaManagerApproveState()
        {
            return this.MediaManagerApproved;
        }

        public ArticleStates.ArticleState getPublishedState()
        {
            return this.Published;
        }

    }
}
