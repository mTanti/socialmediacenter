﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataLayer
{
    public class CommentActions : ConnectionClass
    {
        public CommentActions()
        {
            entity = this.getConnectionClass();
        }

        // Create
        public int postComment(Comment c)
        {
            if (new ArticleActions().getArticle(c.articleid) == null) { return 0 ; }
            if (new UserActions().getUser(c.author) == null) { return 0; }

            entity.Comments.Add(c);
            return entity.SaveChanges();
        }

        // Read
        public Comment getComment(int commentId) {
            return entity.Comments.SingleOrDefault(c => c.commentid == commentId);
        }

        // Update
        public int updateComment(int commentId, Comment newComment) {
            Comment commentToUpdate = getComment(commentId);
            commentToUpdate.comment1 = newComment.comment1;
            return entity.SaveChanges();
        }

        // Delete
        public int deleteComment(int commentId) {
            Comment commentToDelete = getComment(commentId);
            entity.Comments.Remove(commentToDelete);
            return entity.SaveChanges();
        }

        // List
        public IEnumerable<Comment> getCommentsForArticle(int articleId) {
            return entity.Comments.Where(c => c.articleid == articleId);
        }

        public IEnumerable<Comment> getComments() {
            return entity.Comments;
        }
    }
}
