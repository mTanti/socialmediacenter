﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataLayer
{
    public class WorkflowActions : ConnectionClass
    {
        public WorkflowActions()
        {
            entity = this.getConnectionClass();
        }

        public IEnumerable<Workflow> getWorkflows() {
            return entity.Workflows;
        }

        public Workflow getWorkflowById(int workflowId) {
            return entity.Workflows.SingleOrDefault(w => w.workflow_id == workflowId);
        }

        public int getIntialStateId(int workflowId) {
            return entity.Workflows.SingleOrDefault(w => w.workflow_id == workflowId).initial_state;
        }

        public ArticleStateTransition getTransitionEndPointforAction(string action, int workflowId, int stateId) {
           return entity.ArticleStateTransitions.SingleOrDefault(t => t.workflow_id == workflowId && t.StateTransitionAction.action_name == action.ToLower() && t.stateid == stateId);
        }

        public int getArticleWorkflowId(int articleId) {
            return entity.Articles.SingleOrDefault(a => a.articleid == articleId).workflow_id;
        }
    }
}
