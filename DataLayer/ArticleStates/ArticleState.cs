﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.ArticleStates
{
    public abstract class ArticleState
    {
        public int stateId;
        public string stateName;
        public string action;

        public abstract void writerApprove(int workflowId);
        public abstract void mediaManagerApprove(int workflowId);
        public abstract void reject(int workflowId);
        public abstract void publish(int workflowId);
        public abstract void submit(int workflowId);
    }
}
