﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.ArticleStates
{
    public class Rejected : ArticleState
    {
        StatefulArticle sArticle;

        public Rejected(StatefulArticle article) {
            this.sArticle = article;
            this.stateId = 5;
            this.stateName = "Rejected";
        }

        public override void mediaManagerApprove(int workflowId)
        {
            ArticleStateTransition t = new WorkflowActions().getTransitionEndPointforAction("mediamanager approve", workflowId, stateId);
            ArticleStates.ArticleState nextState = sArticle.allStates.SingleOrDefault(s => s.stateId == t.can_transition_to);
            sArticle.setArticleState(nextState);
        }

        public override void publish(int workflowId)
        {
            ArticleStateTransition t = new WorkflowActions().getTransitionEndPointforAction("publish", workflowId, stateId);
            ArticleStates.ArticleState nextState = sArticle.allStates.SingleOrDefault(s => s.stateId == t.can_transition_to);
            sArticle.setArticleState(nextState);
        }

        public override void reject(int workflowId)
        {
            ArticleStateTransition t = new WorkflowActions().getTransitionEndPointforAction("reject", workflowId, stateId);
            ArticleStates.ArticleState nextState = sArticle.allStates.SingleOrDefault(s => s.stateId == t.can_transition_to);
            sArticle.setArticleState(nextState);
        }

        public override void writerApprove(int workflowId)
        {
            ArticleStateTransition t = new WorkflowActions().getTransitionEndPointforAction("writer approve", workflowId, stateId);
            ArticleStates.ArticleState nextState = sArticle.allStates.SingleOrDefault(s => s.stateId == t.can_transition_to);
            sArticle.setArticleState(nextState);
        }

        public override void submit(int workflowId)
        {
            ArticleStateTransition t = new WorkflowActions().getTransitionEndPointforAction("submit", workflowId, stateId);
            ArticleStates.ArticleState nextState = sArticle.allStates.SingleOrDefault(s => s.stateId == t.can_transition_to);
            sArticle.setArticleState(nextState);
        }
    }
}
