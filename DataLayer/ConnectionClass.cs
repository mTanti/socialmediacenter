﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataLayer
{
    public class ConnectionClass
    {
        protected socialmediacenterDBEntities entity { get; set; }
        protected bool instanceFlag = false;

        protected ConnectionClass() { }

            public socialmediacenterDBEntities getConnectionClass() { 
            if (instanceFlag == false){
                entity = new socialmediacenterDBEntities();
                return entity;
            }
            else{
                return entity;
            }
        }
    }
}
