﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataLayer
{
    public class UserActions : ConnectionClass
    {
        public UserActions() {
            entity = this.getConnectionClass();
        }

        // Create
        public int addUser(User u){
            entity.Users.Add(u);
            return entity.SaveChanges();
        }

        // Read
        public User getUser(string username){
            return entity.Users.SingleOrDefault(u => u.username == username);
        }

        //Update
        public int updateUser(string username, User u) {
            User userToUpdate = getUser(username);

            if (userToUpdate == null) { return 0; }

            userToUpdate.name = u.name;
            userToUpdate.surname = u.surname;
            userToUpdate.email = u.email;
            userToUpdate.password = u.password;
            userToUpdate.Roles = u.Roles;

            return entity.SaveChanges();
        }

        // Delete
        public int deleteUser(string username) {
            User userToDelete = getUser(username);

            entity.Users.Remove(userToDelete);
            return entity.SaveChanges();
        }

        // List
        public IEnumerable<User> getUsers(){
            return entity.Users;
        }



        public IEnumerable<User> getUsersByRole(int roleId) {
            List<User> allUsers = getUsers().ToList();
            List<User> toReturn = new List<User>();

            foreach (var user in allUsers)
            {
                if (user.Roles.Where(r => r.roleid == roleId).Count() > 0) {
                    toReturn.Add(user);
                }
            }

            return toReturn;
        }

        

        public bool DoesUsernameExist(string username)
        {
            if (entity.Users.SingleOrDefault(u => u.username == username) == null)
                return false;
            else return true;
        }

        public bool AuthenticateUser(string username, string password)
        {
            return entity.Users.Count(u => u.username == username && u.password == password) > 0 ? true : false;
        }

        public IEnumerable<Role> getUserRole(string username) {
            return entity.Users.SingleOrDefault(u => u.username == username).Roles;
        }

        public IEnumerable<Menu> getUserMenus(string username) {
            Role userrole = entity.Users.SingleOrDefault(u => u.username == username).Roles.First();

            IEnumerable<Menu> menus = entity.Menus.Where(m => m.Roles.Any(r => r.roleid >= userrole.roleid));

            return menus;
        }

        public IEnumerable<Article> getArticlesByUser(string username) {
            return entity.Articles.Where(a => a.author == username);
        }

        // Set
        
        public Role getRole(int roleId) {
            return entity.Roles.SingleOrDefault(r => r.roleid == roleId);
        }


    }
}
