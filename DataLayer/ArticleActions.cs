﻿using Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class ArticleActions : ConnectionClass
    {

        // stateful Articles come in and out.. all methods must convert to article before submitting.

        public ArticleActions()
        {
            entity = this.getConnectionClass();
        }

        // Create
        public int addArticle(StatefulArticle a)
        {

            Article article = new Article();

            article.article_title = a.article_title;
            article.article_text = a.article_text;
            article.author = a.author;
            article.date_posted = DateTime.Now;
            article.state = a.articalState.stateId;
            article.workflow_id = a.workflow_id;

            entity.Articles.Add(article);
            return entity.SaveChanges();
        }

        // Read 
        public StatefulArticle getArticle(int articleid)
        {
            Article article = entity.Articles.SingleOrDefault(a => a.articleid == articleid);
            if (article == null)
            {
                return null;
            }
            else {
                StatefulArticle sa = new StatefulArticle(article, false);
                return sa;
            }
            
        }

        // Update
        public int updateArticle(int articleId, Article a)
        {

            Article article = getArticle(articleId);

            article.article_title = a.article_title;
            article.article_text = a.article_text;
            article.author = a.author;
            article.date_posted = a.date_posted;
            article.state = a.state;

            return entity.SaveChanges();
        }

        public int updateArticle(int articleId, StatefulArticle sa)
        {
            Article article = entity.Articles.SingleOrDefault(a => a.articleid == articleId);

            article.article_title = sa.article_title;
            article.article_text = sa.article_text;
            article.author = sa.author;
            article.date_posted = sa.date_posted;
            article.state = sa.articalState.stateId;

           return entity.SaveChanges();
        }

        // Delete
        public int deleteArticle(int articleId) {
            Article articleToDelete = entity.Articles.SingleOrDefault(a => a.articleid == articleId);

            entity.Articles.Remove(articleToDelete);
            return entity.SaveChanges();
        }

        // List
        public IEnumerable<StatefulArticle> getArticles()
        {
            IEnumerable<Article> articles = entity.Articles;
            List<StatefulArticle> toReturn = new List<StatefulArticle>();
            foreach (var article in articles)
            {
                toReturn.Add(new StatefulArticle(article, false));
            }
            return toReturn;
        }

        public IEnumerable<StatefulArticle> getUserArticles(string username)
        {
            IEnumerable<Article> articles = entity.Articles.Where(a => a.author == username);
            List<StatefulArticle> toReturn = new List<StatefulArticle>();
            foreach (var article in articles)
            {
                toReturn.Add(new StatefulArticle(article, false));
            }
            return toReturn;
        }

        public IEnumerable<StatefulArticle> filterByTitle(string title)
        {
            IEnumerable<Article> articles = entity.Articles.Where(a => a.article_title.Contains(title));
            List<StatefulArticle> toReturn = new List<StatefulArticle>();
            foreach (var article in articles)
            {
                toReturn.Add(new StatefulArticle(article, false));
            }
            return toReturn;
        }

        public IEnumerable<StatefulArticle> getUnreviewedArticles(string username)
        {
            IEnumerable<Article> articles = entity.Articles.Where(a => a.state == 1 && a.author != username);
            List<StatefulArticle> toReturn = new List<StatefulArticle>();
            foreach (var article in articles)
            {
                toReturn.Add(new StatefulArticle(article, false));
            }
            return toReturn;
        }

        public IEnumerable<StatefulArticle> getWriterUnreviewedArticles(string username)
        {
            IEnumerable<Article> articles = entity.Articles.Where(a => a.state == 1 && a.author != username);
            List<StatefulArticle> toReturn = new List<StatefulArticle>();
            foreach (var article in articles)
            {
                toReturn.Add(new StatefulArticle(article, false));
            }
            return toReturn;
        }

        public IEnumerable<StatefulArticle> getMMUnreviewedArticles(string username)
        {
            IEnumerable<Article> articles = entity.Articles.Where(a => a.state == 2 && a.author != username);
            List<StatefulArticle> toReturn = new List<StatefulArticle>();
            foreach (var article in articles)
            {
                toReturn.Add(new StatefulArticle(article, false));
            }
            return toReturn;
        }


        public void postComment(Comment c)
        {
            entity.Comments.Add(c);
            entity.SaveChanges();
        }

        public ArticleState getState(int stateId) {
            return entity.ArticleStates.SingleOrDefault(s => s.stateid == stateId);
        }

        public ArticleState getArticleState(int articleId) {
            Article a = entity.Articles.SingleOrDefault(article => article.articleid == articleId);
            return entity.ArticleStates.SingleOrDefault(s => s.stateid == a.state);
        }

        public Article getArticleByTitle(string title) {
            return entity.Articles.SingleOrDefault(a => a.article_title == title);
        }

        public Article getArticleByBody(string body)
        {
            return entity.Articles.Where(a => a.article_text.Equals(body)).FirstOrDefault();
        }
    }
}
