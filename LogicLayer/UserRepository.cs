﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DataLayer;

namespace LogicLayer
{
    public class UserRepository
    {
        private UserActions ua;

        public UserRepository()
        {
            ua = new UserActions();
        }

        public IEnumerable<User> getUsers()
        {
            return ua.getUsers();
        }

        public IEnumerable<User> getUsersBy
            
            (int roleId) {
            return ua.getUsersByRole(roleId);
        }

        public User getUser(string username)
        {
            return ua.getUser(username);
        }

        public bool DoesUsernameExist(string username)
        {
            return ua.DoesUsernameExist(username);
        }


        public bool AuthenticateUser(string username, string password)
        {
            return ua.AuthenticateUser(username, password);
        }

        public IEnumerable<Role> getUserRole(string username)
        {
            return ua.getUserRole(username);
        }

        public IEnumerable<Menu> getUserMenus(string username)
        {
            return ua.getUserMenus(username);
        }

        public IEnumerable<StatefulArticle> getArticlesByUser(string username)
        {
            IEnumerable<Article> articles = ua.getArticlesByUser(username);
            List<StatefulArticle> toReturn = new List<StatefulArticle>();
            foreach (var article in articles)
            {
                toReturn.Add(new StatefulArticle(article, false));
            }

            return toReturn;
        }

        public Role getRole(int roleId) {
            return ua.getRole(roleId);
        }

        public int addUser(Models.Model_UserCreate model)
        {
            if (ua.DoesUsernameExist(model.username))
            {
                return 0;
            }

            if (model.name.Length > 50) { return 0; }
            else if (model.name.Length < 1) { return 0; }
            else if (model.name == "") { return 0; }
            else if (model.name == null) { return 0; }

            else {
                User u = new User();
                Role r = getRole(model.roleId);
                if (r == null) { return 0; }
                List<Role> userRoles = new List<Role>();
                userRoles.Add(r);

                u.name = model.name;
                u.surname = model.surname;
                u.username = model.username;
                u.password = model.password;
                u.email = model.email;
                u.Roles = userRoles;
                return ua.addUser(u);
            }
        }

        public int deleteUser(string username) {
            return ua.deleteUser(username);
        }

        public int updateUser(string username, User u) {
            if (u.surname.Length > 50) { return 0; }
            if (u.surname.Length < 1) { return 0; }

            return ua.updateUser(username, u);
        }
    }
}
