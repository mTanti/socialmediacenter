﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using Common;

namespace LogicLayer
{
    public class WorkflowRepository
    {
        private WorkflowActions wa;

        public WorkflowRepository()
        {
            wa = new WorkflowActions();
        }

        public IEnumerable<Workflow> getWorkflows() {
            return wa.getWorkflows();
        }

        public Workflow getWorkflowById(int workflowId) {
            return wa.getWorkflowById(workflowId);
        }

        public int getInitialStateId(int workflowId) {
            return wa.getIntialStateId(workflowId);
        }

    }
}
