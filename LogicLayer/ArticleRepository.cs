﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DataLayer;
using LogicLayer.Models;

namespace LogicLayer
{
    public class ArticleRepository
    {
        private ArticleActions aa;

        public ArticleRepository() {
            aa = new ArticleActions();
        }

        // Create
        public int createArticle(Article newArticle)
        {
            StatefulArticle sa = new StatefulArticle(newArticle, true);
            return aa.addArticle(sa);
        }

        // Read
        public Article getArticle(int articleid)
        {
            StatefulArticle sa = aa.getArticle(articleid);
            Article a = new Article();
            a.articleid = sa.articleid;
            a.article_title = sa.article_title;
            a.date_posted = sa.date_posted;
            a.article_text = sa.article_text;
            a.author = sa.author;
            a.workflow_id = sa.workflow_id;
            a.state = sa.state;
            
            return a;
        }

        // Update
        public int updateArticle(Model_ArticleUpdate model)
        {
            StatefulArticle a = new StatefulArticle();

            a.author = model.author;
            a.date_posted = model.date_posted;
            a.article_title = model.title;
            a.article_text = model.body;
            a.workflow_id = model.workflow;
            a.articalState = a.allStates.SingleOrDefault(s => s.stateId == model.state);

            a.articalState.submit(model.workflow);
            return aa.updateArticle(model.articleId, a);
        }

        // Delete
        public int deleteArticle(int articleId)
        {
            return aa.deleteArticle(articleId);
        }

        // List
        public IEnumerable<StatefulArticle> getArticles() {
            return aa.getArticles();
        }


        public IEnumerable<StatefulArticle> getUserArticles(string username) {
            return aa.getUserArticles(username);
        }

        private StatefulArticle getSarticle(int articleId) {
            return aa.getArticle(articleId);
        }

        public IEnumerable<StatefulArticle> filterByTitle(string title)
        {
            return aa.filterByTitle(title);
        }

        public IEnumerable<StatefulArticle> getUnreviewedArticles(string username)
        {
            return aa.getUnreviewedArticles(username);
        }

        public IEnumerable<StatefulArticle> getWriterUnreviewedArticles(string username)
        {
            return aa.getWriterUnreviewedArticles(username);
        }

        public IEnumerable<StatefulArticle> getMMUnreviewedArticles(string username)
        {
            return aa.getMMUnreviewedArticles(username);
        }

        public void Review(int articleId, Comment comment, bool accepted) {
            DataLayer.StatefulArticle reviewed = getSarticle(articleId);
            int workflowId = new WorkflowActions().getArticleWorkflowId(articleId);

            if (accepted){
                if (reviewed.state == 2)
                {
                    reviewed.articalState.mediaManagerApprove(workflowId);
                    reviewed.articalState.publish(workflowId);
                }
                else if (reviewed.state == 1)
                {
                    reviewed.articalState.writerApprove(workflowId);
                }
            }else {
                reviewed.articalState.reject(workflowId);
            }

            aa.postComment(comment);
            aa.updateArticle(articleId, reviewed);
        }

        public void addComment(int articleId, string comment) {
            Comment c = new Comment();
            c.articleid = articleId;
            c.comment1 = comment;

            aa.postComment(c);
        }

        public ArticleState getState(int stateId)
        {
            return aa.getState(stateId);
        }

        public Article getArticleByTitle(string title)
        {
            return aa.getArticleByTitle(title);
        }

        public Article getArticleByBody(string body)
        {
            return aa.getArticleByBody(body);
        }
    }
}
