﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace LogicLayer.Decorator
{
    public class BaseArticle : DecoratorArticle
    {
        private Article article;

        public BaseArticle(string user, int workflow) {
            Workflow w = new LogicLayer.WorkflowRepository().getWorkflowById(workflow);
            ArticleState s = new LogicLayer.ArticleRepository().getState(new LogicLayer.WorkflowRepository().getInitialStateId(workflow));

            article = new Article();
            article.author = user;
            article.date_posted = DateTime.Now;
            article.workflow_id = w.workflow_id;
            article.state = s.stateid;
        }

        public override Article getArticle()
        {
            return article;
        }
    }
}
