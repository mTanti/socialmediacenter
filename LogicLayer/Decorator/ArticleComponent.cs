﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace LogicLayer.Decorator
{
    public abstract class ArticleComponent : DecoratorArticle
    {
        protected DecoratorArticle tempArticle;

        public ArticleComponent(DecoratorArticle newArticle) {
            tempArticle = newArticle;
            this.article = newArticle.getArticle();
        }

        public override Article getArticle()
        {
            return this.article;
        }
    }
}
