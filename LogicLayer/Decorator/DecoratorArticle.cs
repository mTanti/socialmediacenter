﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer.Decorator
{
    public abstract class DecoratorArticle
    {
        protected Article article;

        public abstract Article getArticle();
    }
}
