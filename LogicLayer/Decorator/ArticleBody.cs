﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace LogicLayer.Decorator
{
    public class ArticleBody : ArticleComponent
    {
        private string body;

        public ArticleBody(DecoratorArticle newArticle, string body) : base(newArticle)
        {
            this.body = body;
            this.article.article_text = body;
        }

        public Article getArticle() {
            return this.article;
        }
    }
}
