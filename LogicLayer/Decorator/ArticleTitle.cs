﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace LogicLayer.Decorator
{
    public class ArticleTitle : ArticleComponent
    {
        private string title;

        public ArticleTitle(DecoratorArticle newArticle, string title) : base(newArticle){
            this.title = title;
            this.article.article_title = title;
        }

        public Article getArticle() {
            return this.article;
        }
    }
}
