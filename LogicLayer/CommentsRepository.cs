﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DataLayer;

namespace LogicLayer
{
    public class CommentsRepository
    {
        private CommentActions ca;

        public CommentsRepository()
        {
            ca = new CommentActions();
        }

        // Create
        public int  postComment(Comment c)
        {
            if (c.comment1.Length > 255) { return 0; }
            if (c.comment1.Length < 1) { return 0; }

            return ca.postComment(c);
        }

        // Read
        public Comment getComment(int commentId)
        {
            return ca.getComment(commentId);
        }

        // Update
        public int updateComment(int commentId, Comment newComment)
        {
            if (newComment.comment1.Length > 255) { return 0; }
            if (newComment.comment1.Length < 1) { return 0; }
            
            return ca.updateComment(commentId, newComment);
        }
   
        // Delete
        public int deleteComment(int commentId)
        {
            return ca.deleteComment(commentId);
        }

        // List
        public IEnumerable<Comment> getCommentsForArticle(int articleId)
        {
            return ca.getCommentsForArticle(articleId);
        }

        public IEnumerable<Comment> getComments()
        {
            return ca.getComments();
        }
    }
}
