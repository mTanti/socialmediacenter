﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace LogicLayer.AbstractFactory
{
    public abstract class ArticleFactory
    {
        public abstract Article createArticle(Models.Model_ArticleCreate model, string name);
    }
}
