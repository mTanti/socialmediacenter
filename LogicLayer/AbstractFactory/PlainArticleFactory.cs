﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using LogicLayer.Decorator;

namespace LogicLayer.AbstractFactory
{
    public class PlainArticleFactory : ArticleFactory
    {
        public override Article createArticle(Models.Model_ArticleCreate model, string name)
        {
            DecoratorArticle d = new ArticleBody(new ArticleTitle(new BaseArticle(name, model.workflow), model.article_title), model.article_text);
            return d.getArticle();
        }
    }
}
