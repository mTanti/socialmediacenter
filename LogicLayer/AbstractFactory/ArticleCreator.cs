﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace LogicLayer.AbstractFactory
{
    public class ArticleCreator
    {
        private Article createdArticle;
        private ArticleFactory factory;

        public ArticleCreator(ArticleFactory factory, Models.Model_ArticleCreate model, string name) {
            this.factory = factory;
            this.createdArticle = factory.createArticle(model, name);
        }

        public Article getArticle() {
            return this.createdArticle;
        }
    }
}
