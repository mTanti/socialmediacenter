﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace LogicLayer.Models
{
    public class Model_ArticleCreate
    {

        [Display(Name = "Title")]
        public string article_title { get; set; }

        [Required]
        [Display(Name = "licence")]
        public string lincense { get; set; }

        [Required]
        [Display(Name = "Text")]
        public string article_text { get; set; }

        [Required]
        [Display(Name = "Author")]
        public string author { get; set; }

        [Required]
        [Display(Name = "Workflow")]
        public int workflow { get; set; }

        
    }
}
