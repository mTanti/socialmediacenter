﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LogicLayer.Models
{
    public class Model_CommentEdit
    {
        [Required]
        [Display(Name = "CommentId")]
        public int commentId { get; set; }

        [Required]
        [Display(Name = "Comment")]
        public string comment { get; set; }
    }
}
