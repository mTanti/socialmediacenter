﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer.Models
{
    public class Model_ArticleDelete
    {
        [Required]
        [Display(Name = "Confirmation")]
        public bool confirm { get; set; }

        [Required]
        [Display(Name = "ArticleId")]
        public int articleId { get; set; }
    }
}
