﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LogicLayer.Models
{
    public class Model_CommentDelete
    {
        [Required]
        [Display(Name = "Confirmation")]
        public bool confirm { get; set; }

        [Required]
        [Display(Name = "CommentId")]
        public int commentId { get; set; }
    }
}
