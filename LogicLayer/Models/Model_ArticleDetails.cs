﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LogicLayer.Models
{
    public class Model_ArticleDetails
    {
        [Required]
        [Display(Name = "Article Id")]
        public int articleId { get; set; }
    }
}
