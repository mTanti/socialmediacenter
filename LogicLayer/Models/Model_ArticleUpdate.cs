﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer.Models
{
    public class Model_ArticleUpdate
    {
        [Required]
        [Display(Name = "ArticleId")]
        public int articleId { get; set; }

        [Required]
        [Display(Name = "Author")]
        public string author { get; set; }

        [Display(Name = "Title")]
        public string title { get; set; }

        [Required]
        [Display(Name = "Text")]
        public string body { get; set; }

        [Required]
        [Display(Name = "State")]
        public int state { get; set; }

        [Required]
        [Display(Name = "Workflow")]
        public int workflow { get; set; }

        [Required]
        [Display(Name = "Date Posted")]
        public DateTime date_posted { get; set; }
    }
}
