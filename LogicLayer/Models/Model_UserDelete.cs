﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LogicLayer.Models
{
    public class Model_UserDelete
    {
        [Required]
        [Display(Name = "Confirmation")]
        public bool confirm { get; set; }

        [Required]
        [Display(Name = "username")]
        public string username { get; set; }
    }
}
