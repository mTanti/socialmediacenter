﻿using Common;
using SocialMediaCenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LogicLayer.Decorator;
using LogicLayer.AbstractFactory;
using LogicLayer.Models;
using LogicLayer;

namespace SocialMediaCenter.Controllers
{
    public class ArticleController : Controller
    {
        // GET: Article
        [Authorize(Roles = "Writer, MediaManager, Admin")]
        public ActionResult Index()
        {
            return View();
        }

        // GET: Article/Details/5
        [Authorize(Roles = "Writer, MediaManager, Admin")]
        public ActionResult Details(int articleId)
        {
            Model_ArticleDetails model = new Model_ArticleDetails();
            model.articleId = articleId;
            return View(model);
        }

        // GET: Article/Create
        [Authorize(Roles = "Writer, MediaManager")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Article/Create
        [HttpPost]
        public ActionResult Create(LogicLayer.Models.Model_ArticleCreate model)
        {
            try
            {
                Article newArticle;
                if (model.article_title == "")
                {
                    ArticleCreator notTitle = new ArticleCreator(new NoTitleArticleFactory(), model, User.Identity.Name);
                    newArticle = notTitle.getArticle();
                }
                else {
                    ArticleCreator plainArticle = new ArticleCreator(new PlainArticleFactory(), model, User.Identity.Name);
                    newArticle = plainArticle.getArticle();
                }
                
                
                new LogicLayer.ArticleRepository().createArticle(newArticle);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Article/Edit/5
        [Authorize(Roles = "Writer, MediaManager")]
        public ActionResult Edit(int articleId)
        {
            Model_ArticleUpdate model = new Model_ArticleUpdate();
            model.articleId = articleId;
            return View(model);
        }

        // POST: Article/Edit/5
        [HttpPost]
        public ActionResult Edit(Model_ArticleUpdate model)
        {
            try
            {
                Article a = new ArticleRepository().getArticle(model.articleId);
                model.author = a.author;
                model.state = a.state;
                model.workflow = a.workflow_id;
                new ArticleRepository().updateArticle(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [Authorize(Roles = "Writer, MediaManager")]
        public ActionResult Review(int articleId) {
            LogicLayer.Models.Model_ArticleReview model = new LogicLayer.Models.Model_ArticleReview();

            model.articleId = articleId;
            return View(model);
        }

        [HttpPost]
        public ActionResult Review(LogicLayer.Models.Model_ArticleReview model) {
            try
            {
                Comment c = new Comment();
                c.articleid = model.articleId;
                c.comment1 = model.comment;
                c.author = User.Identity.Name;

                new LogicLayer.ArticleRepository().Review(model.articleId, c, model.accepted);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Article/Delete/5
        [Authorize(Roles = "Writer, MediaManager")]
        public ActionResult Delete(int articleId)
        {
            Model_ArticleDelete model = new Model_ArticleDelete();
            model.articleId = articleId;

            return View(model);
        }

        // POST: Article/Delete/5
        [HttpPost]
        public ActionResult Delete(Model_ArticleDelete model)
        {
            try
            {
                if (model.confirm)
                {
                    new ArticleRepository().deleteArticle(model.articleId);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
