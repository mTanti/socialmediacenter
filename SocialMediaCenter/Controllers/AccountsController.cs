﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LogicLayer;
using Common;
using LogicLayer.Models;

namespace SocialMediaCenter.Controllers
{
    public class AccountsController : Controller
    {
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View();
        }

        // GET: Accounts/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int articleId)
        {
            return View();
        }

        // GET: Accounts/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Accounts/Create
        [HttpPost]
        public ActionResult Create(LogicLayer.Models.Model_UserCreate model)
        {
            try
            {
                new LogicLayer.UserRepository().addUser(model);         
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Accounts/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(string username)
        {
            Model_UserUpdate model = new Model_UserUpdate();
            model.username = username;
            return View(model);
        }

        // POST: Accounts/Edit/5
        [HttpPost]
        public ActionResult Edit(Model_UserUpdate model)
        {
            try
            {
                User u = new Common.User();
                u.name = model.name;
                u.surname = model.surname;
                u.email = model.email;
                u.password = model.password;

                new UserRepository().updateUser(model.username, u);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Accounts/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string username)
        {
            Model_UserDelete model = new Model_UserDelete();
            model.username = username;
            return View(model);
        }

        // POST: Accounts/Delete/5
        [HttpPost]
        public ActionResult Delete(Model_UserDelete model)
        {
            try
            {
                if (model.confirm) {
                    new UserRepository().deleteUser(model.username);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
