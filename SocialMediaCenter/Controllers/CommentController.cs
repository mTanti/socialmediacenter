﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LogicLayer;
using LogicLayer.Models;
using Common;

namespace SocialMediaCenter.Controllers
{
    public class CommentController : Controller
    {
        // GET: Comment
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(int commentId) {

            Model_CommentEdit model = new Model_CommentEdit();

            model.commentId = commentId;

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Model_CommentEdit model) {
            
            try
            {
                Comment c = new Comment();
                c.comment1 = model.comment;
                new LogicLayer.CommentsRepository().updateComment(model.commentId, c);
                return RedirectToAction("Index", "Article");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int commentId)
        {

            Model_CommentDelete model = new Model_CommentDelete();

            model.commentId = commentId;

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(Model_CommentDelete model)
        {

            try
            {
                if (model.confirm)
                {
                    new CommentsRepository().deleteComment(model.commentId);
                }
                return RedirectToAction("Index", "Article");
            }
            catch
            {
                return View();
            }
        }
    }
}