﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SocialMediaCenter.Models
{
    public class ArticleCreateModel
    {
        [Required]
        [Display(Name = "Title")]
        public string articleTitle { get; set; }

        [Required]
        [Display(Name = "License")]
        public string lincense { get; set; }

        [Required]
        [Display(Name = "Article Text")]
        public string articleText { get; set; }
    }

    public class ArticleEditModel {

        [Display(Name = "Title")]
        public string articleTitle { get; set; }

        [Display(Name = "Article Text")]
        public string articleText { get; set; }
    }

    public class ArticleShowModel {
        [Display(Name = "ID")]
        public int articleId { get; set; }

        [Display(Name = "Title")]
        public string articleTitle { get; set; }

        [Display(Name = "Author")]
        public string author { get; set; }

        [Display(Name = "DatePosted")]
        public DateTime datePosted { get; set; }
    }

    public class ArticleReviewModel {
        [Display(Name = "ID")]
        public int articleId { get; set; }

        [Display(Name = "Comment")]
        public string comment { get; set; }
    }
}