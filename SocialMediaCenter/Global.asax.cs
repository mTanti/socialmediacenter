﻿using Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SocialMediaCenter
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e) {
            if (Context.User != null) {
                IEnumerable<Role> roles = new LogicLayer.UserRepository().getUserRole(Context.User.Identity.Name);
                GenericPrincipal contextUser = new GenericPrincipal(User.Identity, roles.Select<Role, string>(x => x.role_name).ToArray());
                Context.User = contextUser;
            }
        }
    }
}
