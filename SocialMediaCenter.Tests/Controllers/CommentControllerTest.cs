﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Transactions;
using LogicLayer;
using LogicLayer.Models;
using Common;

namespace SocialMediaCenter.Tests.Controllers
{

    [TestClass]
    public class CommentControllerTest
    {
        [TestInitialize]
        public void setUp()
        {

        }

        [TestMethod] // SuccessPath
        public void createCommentTestCase1()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = new CommentsRepository().postComment(c);

                    IEnumerable<Comment> comments = new CommentsRepository().getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "testComment");
                    Assert.AreEqual(success, 1);

                    Assert.AreNotEqual(createdComment, null);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod] // SuccessPath
        public void createCommentTestCase2()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "matthew";
                    c.articleid = 5000;
                    int success = new CommentsRepository().postComment(c);

                    Assert.AreEqual(success, 0);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod] // SuccessPath
        public void createCommentTestCase3()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Comment c = new Comment();
                    c.comment1 = "";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = new CommentsRepository().postComment(c);
                    Assert.AreEqual(success, 0);

                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod] // SuccessPath
        public void createCommentTestCase4()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Comment c = new Comment();
                    c.comment1 = "c";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = new CommentsRepository().postComment(c);

                    IEnumerable<Comment> comments = new CommentsRepository().getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "c");
                    Assert.AreEqual(success, 1);

                    Assert.AreNotEqual(createdComment, null);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod] // SuccessPath
        public void createCommentTestCase5()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Comment c = new Comment();
                    c.comment1 = "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia non asdsadsadasfasf";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = new CommentsRepository().postComment(c);
                    Assert.AreEqual(success, 0);

                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod] // SuccessPath
        public void createCommentTestCase6()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Comment c = new Comment();
                    c.comment1 = "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia non";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = new CommentsRepository().postComment(c);

                    IEnumerable<Comment> comments = new CommentsRepository().getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia non");
                    Assert.AreEqual(success, 1);

                    Assert.AreNotEqual(createdComment, null);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod] // SuccessPath
        public void createCommentTestCase7()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Comment c = new Comment();
                    c.comment1 = "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia no";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = new CommentsRepository().postComment(c);

                    IEnumerable<Comment> comments = new CommentsRepository().getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia no");
                    Assert.AreEqual(success, 1);

                    Assert.AreNotEqual(createdComment, null);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod] // SuccessPath
        public void createCommentTestCase8()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Comment c = new Comment();
                    c.comment1 = "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia nons";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = new CommentsRepository().postComment(c);
                    Assert.AreEqual(success, 0);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod] // SuccessPath
        public void createCommentTestCase9()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "Non Existent";
                    c.articleid = 2;
                    int success = new CommentsRepository().postComment(c);
                    Assert.AreEqual(success, 0);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod] // SuccessPath
        public void deleteCommentTest()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {

                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = new CommentsRepository().postComment(c);
                    Assert.AreEqual(success, 1);

                    IEnumerable<Comment> comments = new CommentsRepository().getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "testComment");

                    success = new CommentsRepository().deleteComment(createdComment.commentid);
                    Assert.AreEqual(success, 1);

                    comments = new CommentsRepository().getCommentsForArticle(2);
                    Comment deletedComment = comments.SingleOrDefault(com => com.comment1 == "testComment");
                    Assert.AreEqual(deletedComment, null);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod] // SuccessPath
        public void updateCommentTestCase1()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    CommentsRepository repo = new CommentsRepository();
                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = repo.postComment(c);
                    Assert.AreEqual(success, 1);

                    IEnumerable<Comment> comments = repo.getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "testComment");

                    createdComment.comment1 = "Edited Comment";
                    success = repo.updateComment(createdComment.commentid, createdComment);

                    comments = repo.getCommentsForArticle(2);
                    Comment editedComment = comments.SingleOrDefault(com => com.commentid == createdComment.commentid);

                    Assert.AreEqual(editedComment.comment1, "Edited Comment");


                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod] // SuccessPath
        public void updateCommentTestCase2()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    CommentsRepository repo = new CommentsRepository();
                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = repo.postComment(c);
                    Assert.AreEqual(success, 1);

                    IEnumerable<Comment> comments = repo.getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "testComment");

                    createdComment.comment1 = "";
                    success = repo.updateComment(createdComment.commentid, createdComment);
                    Assert.AreEqual(success, 0);


                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod] // SuccessPath
        public void updateCommentTestCase3()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    CommentsRepository repo = new CommentsRepository();
                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = repo.postComment(c);
                    Assert.AreEqual(success, 1);

                    IEnumerable<Comment> comments = repo.getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "testComment");

                    createdComment.comment1 = "c";
                    success = repo.updateComment(createdComment.commentid, createdComment);

                    comments = repo.getCommentsForArticle(2);
                    Comment editedComment = comments.SingleOrDefault(com => com.commentid == createdComment.commentid);

                    Assert.AreEqual(editedComment.comment1, "c");


                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod] // SuccessPath
        public void updateCommentTestCase4()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    CommentsRepository repo = new CommentsRepository();
                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = repo.postComment(c);
                    Assert.AreEqual(success, 1);

                    IEnumerable<Comment> comments = repo.getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "testComment");

                    createdComment.comment1 = "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia nonsadsadf";
                    success = repo.updateComment(createdComment.commentid, createdComment);
                    Assert.AreEqual(success, 0);


                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod] // SuccessPath
        public void updateCommentTestCase5()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    CommentsRepository repo = new CommentsRepository();
                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = repo.postComment(c);
                    Assert.AreEqual(success, 1);

                    IEnumerable<Comment> comments = repo.getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "testComment");

                    createdComment.comment1 = "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia non";
                    success = repo.updateComment(createdComment.commentid, createdComment);

                    comments = repo.getCommentsForArticle(2);
                    Comment editedComment = comments.SingleOrDefault(com => com.commentid == createdComment.commentid);

                    Assert.AreEqual(editedComment.comment1, "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia non");
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod] // SuccessPath
        public void updateCommentTestCase6()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    CommentsRepository repo = new CommentsRepository();
                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = repo.postComment(c);
                    Assert.AreEqual(success, 1);

                    IEnumerable<Comment> comments = repo.getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "testComment");

                    createdComment.comment1 = "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia no";
                    success = repo.updateComment(createdComment.commentid, createdComment);

                    comments = repo.getCommentsForArticle(2);
                    Comment editedComment = comments.SingleOrDefault(com => com.commentid == createdComment.commentid);

                    Assert.AreEqual(editedComment.comment1, "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia no");


                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod] // SuccessPath
        public void updateCommentTestCase7()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    CommentsRepository repo = new CommentsRepository();
                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = repo.postComment(c);
                    Assert.AreEqual(success, 1);

                    IEnumerable<Comment> comments = repo.getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "testComment");

                    createdComment.comment1 = "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia nons";
                    success = repo.updateComment(createdComment.commentid, createdComment);
                    Assert.AreEqual(success, 0);


                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }


        [TestMethod] // SuccessPath
        public void getCommentTest()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Comment c = new Comment();
                    c.comment1 = "testComment";
                    c.author = "matthew";
                    c.articleid = 2;
                    int success = new CommentsRepository().postComment(c);
                    Assert.AreEqual(success, 1);

                    IEnumerable<Comment> comments = new CommentsRepository().getCommentsForArticle(2);
                    Comment createdComment = comments.SingleOrDefault(com => com.comment1 == "testComment");
                    Assert.AreEqual(createdComment.comment1, "testComment");
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }
    }
}
