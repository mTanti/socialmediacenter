﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Transactions;
using LogicLayer;
using LogicLayer.Models;
using Common;

namespace SocialMediaCenter.Tests.Controllers
{
    [TestClass]
    public class ArticleControllerTest
    {
        [TestInitialize]
        public void setUp()
        {
            
        }
        [TestMethod]
        public void createArticleTestCase1() { // TestCase 1

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Article a = new Article();
                    a.article_title = "test";
                    a.author = "matthew";
                    a.article_text = "test";
                    a.workflow_id = 1;
                    a.state = 1;
                    a.workflow_id = 1;
                    a.date_posted = DateTime.Now;

                    int success = new ArticleRepository().createArticle(a);
                    // TODO
                    Article exists = new ArticleRepository().getArticleByTitle("test");
                    Assert.AreEqual(success, 1);
                    Assert.AreEqual(exists.article_text, "test");
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
         }

        [TestMethod]
        public void editArticleTest() { // SuccessPath
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    ArticleRepository repo = new ArticleRepository();
                    Article a = new Article();
                    a.article_title = "test";
                    a.author = "matthew";
                    a.article_text = "test";
                    a.workflow_id = 1;
                    a.state = 1;
                    a.workflow_id = 1;
                    a.date_posted = DateTime.Now;

                    int success = repo.createArticle(a);
                    Assert.AreEqual(success, 1);

                    Article exists = repo.getArticleByTitle("test");

                    Model_ArticleUpdate u = new Model_ArticleUpdate();
                    u.articleId = exists.articleid;
                    u.author = exists.author;
                    u.body = "Edited Body";
                    u.date_posted = exists.date_posted;
                    u.state = exists.state;
                    u.title = exists.article_title;
                    u.workflow = exists.workflow_id;

                    success = repo.updateArticle(u);

                    Article edited = new ArticleRepository().getArticleByTitle("test");
                    Assert.AreEqual(success, 1);
                    Assert.AreEqual(exists.article_text, "Edited Body");


                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }
    

        [TestMethod]
        public void deleteArticleTest() { // SuccessPath
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Article a = new Article();
                    a.article_title = "test";
                    a.author = "matthew";
                    a.article_text = "test";
                    a.workflow_id = 1;
                    a.state = 1;
                    a.workflow_id = 1;
                    a.date_posted = DateTime.Now;

                    int success = new ArticleRepository().createArticle(a);
                    Assert.AreEqual(success, 1);

                    Article exists = new ArticleRepository().getArticleByTitle("test");
                    Assert.AreEqual(exists.article_text, "test");

                    success = new ArticleRepository().deleteArticle(exists.articleid);
                    Assert.AreEqual(success, 1);

                    Article isDeleted = new ArticleRepository().getArticleByTitle("test");
                    Assert.AreEqual(isDeleted, null);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }


        [TestMethod]
        public void getArticleTest() { // SuccessPath
        using (TransactionScope ts = new TransactionScope())
        {
            try
            {
                    Article a = new Article();
                    a.article_title = "test";
                    a.author = "matthew";
                    a.article_text = "test";
                    a.workflow_id = 1;
                    a.state = 1;
                    a.workflow_id = 1;
                    a.date_posted = DateTime.Now;

                    int success = new ArticleRepository().createArticle(a);
                    Assert.AreEqual(success, 1);

                    Article toCompare = new ArticleRepository().getArticleByTitle("test");
                    Assert.AreEqual(toCompare.article_text, "test");
                }
                catch
            {
                Assert.Fail();
                ts.Dispose();
            }
            finally
            {
                ts.Dispose();
            }
        }
    }
    }
}
