﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Transactions;
using LogicLayer;
using Common;

namespace SocialMediaCenter.Tests.Controllers
{
    [TestClass]
    public class UserControllerTest
    {
        private SqlConnection conn;
        private DataTable dtOriginal;

        [TestInitialize]
        public void setUp()
        {
            conn = new SqlConnection();
            conn.ConnectionString =
            "Data Source=MSI\\SQLEXPRESS;" +
            "Initial Catalog=socialmediacenterDB;" +
            "Integrated Security=True;";
            conn.Open();
            dtOriginal = new DataTable("Users");
        }

        [TestMethod]
        public void createUserTestCase1()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User toCompare = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    int success = new UserRepository().addUser(u);

                    // make sure it exists
                    User exists = new UserRepository().getUser("test12");
                    Assert.AreEqual(success, 2);
                    Assert.AreEqual(exists.username, u.username);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod]
        public void createUserTestCase2() // Existing Username
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User toCompare = new User();
                    u.username = "matthew";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    var success = new UserRepository().addUser(u);

                    Assert.AreEqual(success, 0);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod]
        public void createUserTestCase3()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User toCompare = new User();
                    u.username = "test12";
                    u.name = "qwhtmkiuygqwhtmkiuygqwhtmkiuygqwhtmkiuygqwhtmkiuygqwhtmkiuyg";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    int success = new UserRepository().addUser(u);

                    Assert.AreEqual(success, 0);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod]
        public void createUserTestCase4()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User toCompare = new User();
                    u.username = "test12";
                    u.name = "";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    int success = new UserRepository().addUser(u);

                    Assert.AreEqual(success, 0);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod]
        public void createUserTestCase5()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User toCompare = new User();
                    u.username = "test12";
                    u.name = "m";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    int success = new UserRepository().addUser(u);

                    // make sure it exists
                    User exists = new UserRepository().getUser("test12");
                    Assert.AreEqual(success, 2);
                    Assert.AreEqual(exists.username, u.username);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod]
        public void createUserTestCase6()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User toCompare = new User();
                    u.username = "test12";
                    u.name = "qwhtmkiuygqwhtmkiuygqwhtmkiuygqwhtmkiuygqwhtmkiuy";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    int success = new UserRepository().addUser(u);

                    // make sure it exists
                    User exists = new UserRepository().getUser("test12");
                    Assert.AreEqual(success, 2);
                    Assert.AreEqual(exists.username, u.username);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod]
        public void createUserTestCase7()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User toCompare = new User();
                    u.username = "test12";
                    u.name = "qwhtmkiuygqwhtmkiuygqwhtmkiuygqwhtmkiuygqwhtmkiuyg";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    int success = new UserRepository().addUser(u);

                    // make sure it exists
                    User exists = new UserRepository().getUser("test12");
                    Assert.AreEqual(success, 2);
                    Assert.AreEqual(exists.username, u.username);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod]
        public void createUserTestCase8()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User toCompare = new User();
                    u.username = "test12";
                    u.name = "qwhtmkiuygqwhtmkiuygqwhtmkiuygqwhtmkiuygqwhtmkiuygs";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    int success = new UserRepository().addUser(u);

                    Assert.AreEqual(success, 0);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }


        [TestMethod]
        public void createUserTestCase9()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User toCompare = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 6;
                    int success = new UserRepository().addUser(u);

                    Assert.AreEqual(success, 0);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod]
        public void deleteUser() { // SuccessPath
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User toCompare = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    new UserRepository().addUser(u);

                    // make sure it exists
                    User exists = new UserRepository().getUser("test12");
                    Assert.AreEqual(exists.username, u.username);

                    int success = new UserRepository().deleteUser("test12");
                    Assert.AreEqual(success, 1);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }
        }

        [TestMethod]
        public void updateUserTestCase1() {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    UserRepository repo = new UserRepository();
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User user = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    repo.addUser(u);

                    // make sure it exists
                    User exists = repo.getUser("test12");
                    Assert.AreEqual(exists.username, u.username);

                    exists.name = "newName";

                    int success = repo.updateUser("test12", exists);
                    User edited = repo.getUser("test12");
                    Assert.AreEqual(success, 1);
                    Assert.AreEqual(edited.name, "newName");
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

         }
        [TestMethod]
        public void updateUserTestCase2()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    UserRepository repo = new UserRepository();
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User user = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    repo.addUser(u);

                    // make sure it exists
                    User exists = repo.getUser("test12");
                    Assert.AreEqual(exists.username, u.username);

                    exists.name = "newName";

                    int success = repo.updateUser("nonExistent", exists);
                    Assert.AreEqual(success, 0);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod]
        public void updateUserTestCase3()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    UserRepository repo = new UserRepository();
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User user = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    repo.addUser(u);

                    // make sure it exists
                    User exists = repo.getUser("test12");
                    Assert.AreEqual(exists.username, u.username);

                    exists.surname = "qwhtmkiuygqwhtmkiuygqwhtmkiuygqwhtmkiuygqwhtmkiuygssadas";

                    int success = repo.updateUser("test12", exists);
                    Assert.AreEqual(success, 0);

                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod]
        public void updateUserTestCase4()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    UserRepository repo = new UserRepository();
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User user = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    repo.addUser(u);

                    // make sure it exists
                    User exists = repo.getUser("test12");
                    Assert.AreEqual(exists.username, u.username);

                    exists.surname = "t";

                    int success = repo.updateUser("test12", exists);
                    User edited = repo.getUser("test12");
                    Assert.AreEqual(success, 1);
                    Assert.AreEqual(edited.surname, "t");
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod]
        public void updateUserTestCase5()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    UserRepository repo = new UserRepository();
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User user = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    repo.addUser(u);

                    // make sure it exists
                    User exists = repo.getUser("test12");
                    Assert.AreEqual(exists.username, u.username);

                    exists.surname = "qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuio";

                    int success = repo.updateUser("test12", exists);
                    User edited = repo.getUser("test12");
                    Assert.AreEqual(success, 1);
                    Assert.AreEqual(edited.surname, "qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuio");
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod]
        public void updateUserTestCase6()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    UserRepository repo = new UserRepository();
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User user = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    repo.addUser(u);

                    // make sure it exists
                    User exists = repo.getUser("test12");
                    Assert.AreEqual(exists.username, u.username);

                    exists.surname = "qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiop";

                    int success = repo.updateUser("test12", exists);
                    User edited = repo.getUser("test12");
                    Assert.AreEqual(success, 1);
                    Assert.AreEqual(edited.surname, "qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiop");
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod]
        public void updateUserTestCase7()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    UserRepository repo = new UserRepository();
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User user = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    repo.addUser(u);

                    // make sure it exists
                    User exists = repo.getUser("test12");
                    Assert.AreEqual(exists.username, u.username);

                    exists.surname = "qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiops";

                    int success = repo.updateUser("test12", exists);
                    Assert.AreEqual(success, 0);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod]
        public void updateUserTestCase8()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    UserRepository repo = new UserRepository();
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User user = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    repo.addUser(u);

                    // make sure it exists
                    User exists = repo.getUser("test12");
                    Assert.AreEqual(exists.username, u.username);
                    Role toAdd = new UserRepository().getRole(6);

                    Assert.AreEqual(toAdd, null);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }

        [TestMethod]
        public void updateUserTestCase9()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    UserRepository repo = new UserRepository();
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User user = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;
                    repo.addUser(u);

                    // make sure it exists
                    User exists = repo.getUser("test12");
                    Assert.AreEqual(exists.username, u.username);

                    exists.surname = "";

                    int success = repo.updateUser("test12", exists);
                    Assert.AreEqual(success, 0);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally
                {
                    ts.Dispose();
                }
            }

        }


        [TestMethod]
        public void GetUser() { // SuccessPath
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    LogicLayer.Models.Model_UserCreate u = new LogicLayer.Models.Model_UserCreate();
                    User toCompare = new User();
                    u.username = "test12";
                    u.name = "test";
                    u.surname = "test";
                    u.email = "test@test.com";
                    u.password = "test";
                    u.roleId = 3;

                    toCompare.username = "test12";
                    toCompare.name = "test";
                    toCompare.surname = "test";
                    toCompare.email = "test@test.com";
                    toCompare.password = "test";
                    toCompare.Roles.Add(new UserRepository().getRole(3));

                    new UserRepository().addUser(u);
                    User result = new UserRepository().getUser("test12");
                    Assert.AreEqual(toCompare.username, result.username);
                }
                catch
                {
                    Assert.Fail();
                    ts.Dispose();
                }
                finally {
                    ts.Dispose();
                }
            }
        }

        [TestMethod]
        public void GetUserIncorrectUsername() {
            User result = new UserRepository().getUser("test12");
            Assert.AreEqual(null, result);
        }
    }
}
